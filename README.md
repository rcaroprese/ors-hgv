# Docker container creation
To create the docker container follow these steps:
1. Clone this repository
1. Make the following directories:

    `mkdir -p data/elevation_cache data/graphs data/pbf`

1. Generate the custom OSM PBF file and copy it to `data/pbf`
1. Uncomment lines **11** and **12** in the `docker-compose.yml` file and change line **18** to:

    ```- BUILD_GRAPHS=True```
1. Go to the docker directory: 

    `cd docker`
1. Run:

    `docker-compose up`
1. If you want to, you can push the docker container to dockerhub